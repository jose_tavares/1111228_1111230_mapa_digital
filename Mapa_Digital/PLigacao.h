#ifndef PLigacao_
#define PLigacao_

#include <iostream>
using namespace std;

#include "Autoestrada.h"
#include "Ligacao.h"
#include "Nacional.h"

class PLigacao
{
	Ligacao *ape;

	enum TipoComparacao { KMS , CUSTO , TEMPO}; 
	static TipoComparacao  tipoComparacao;
public:
	static void setComparacaoKMS();
	static void setComparacaoCUSTO();
	static void setComparacaoTEMPO();

	int getKM() const;
	double getPreco() const;
	int getTempo() const;
	string getDesc() const;


	PLigacao();
	PLigacao(Ligacao* &e);
	PLigacao(const Ligacao &e);
	PLigacao(string d, int c, int t ,double p);
	PLigacao(string d, int c, int t, string p);
	PLigacao(const PLigacao &e);
	~PLigacao();

	bool operator >(const PLigacao &e) const;
	bool operator <(const PLigacao &e) const;
	bool operator ==(const PLigacao &e) const;
	PLigacao operator+(const PLigacao &e);
	const PLigacao & operator+=(const PLigacao &e);
	const PLigacao & operator=(const PLigacao &e);
	void write(ostream &out) const;
};

PLigacao::TipoComparacao PLigacao::tipoComparacao=PLigacao::TipoComparacao::KMS;

void PLigacao::setComparacaoKMS() {
	tipoComparacao=TipoComparacao::KMS;
}
void PLigacao::setComparacaoCUSTO() {
	tipoComparacao=TipoComparacao::CUSTO;
}

void PLigacao::setComparacaoTEMPO() {
	tipoComparacao=TipoComparacao::TEMPO;
}

int PLigacao::getKM() const {
	return ape->getTotalKm();
}

string PLigacao::getDesc() const {
	return ape->getCodigo();
}

double PLigacao::getPreco() const {
	return ape->getPrecoPortagem();
}

int PLigacao::getTempo() const {
	return ape->getTempoM();
}
PLigacao::PLigacao() {
	this->ape = new Autoestrada();
}
PLigacao::PLigacao(string d, int c, int t ,double p) {
	ape = new Autoestrada( d,  c, t , p);
}


PLigacao::PLigacao(string d, int c, int t ,string p) {
	ape = new Nacional( d,  c, t , p);
}


PLigacao::PLigacao(Ligacao* &e) {
	this->ape = e->clone();
}

PLigacao::PLigacao(const Ligacao &e) {
	this->ape = e.clone();
}
PLigacao::PLigacao(const PLigacao &e) {
	this->ape = e.ape->clone();
}
PLigacao::~PLigacao() {
	delete ape;
}


bool PLigacao::operator >(const PLigacao &e) const {	
	if (tipoComparacao==TipoComparacao::KMS) return (*this).getKM() > e.getKM();
	if (tipoComparacao==TipoComparacao::CUSTO){
		if((*this).getPreco() == e.getPreco()){
			return (*this).getKM() > e.getKM();
		}
		return (*this).getPreco() > e.getPreco();
	}
	return (*this).getTempo() > e.getTempo();
}

bool PLigacao::operator <(const PLigacao &e) const {
	if (tipoComparacao==TipoComparacao::KMS) return (*this).getKM() < e.getKM();
	if (tipoComparacao==TipoComparacao::CUSTO){
		if((*this).getPreco() == e.getPreco()){
			return (*this).getKM() < e.getKM();
		}
		return (*this).getPreco() < e.getPreco();
	}
	return (*this).getTempo() < e.getTempo();
}
bool PLigacao::operator ==(const PLigacao &e) const {
	if (tipoComparacao==TipoComparacao::KMS) return (*this).getKM() == e.getKM();
	if (tipoComparacao==TipoComparacao::CUSTO) return (*this).getPreco() == e.getPreco();
	return (*this).getTempo() == e.getTempo();
}
PLigacao PLigacao::operator+(const PLigacao &e) {
	return PLigacao("", (*this).getKM()+e.getKM(), (*this).getTempo()+e.getTempo(), (*this).getPreco()+e.getPreco());
}
const PLigacao & PLigacao::operator+=(const PLigacao &e) {
	this->ape->setTotalKm(this->ape->getTotalKm()+e.ape->getTotalKm());
	this->ape->setTempoM(this->ape->getTempoM()+e.ape->getTempoM());
	if (typeid(*ape)==typeid(Autoestrada)) {
		Autoestrada *ae = (Autoestrada *)this->ape;
		ae->setPrecoPortagem(ae->getPrecoPortagem()+e.ape->getPrecoPortagem());
	}	
	return  *this;
}
const PLigacao & PLigacao::operator=(const PLigacao &e) {
	this->ape = e.ape->clone();
	return *this;
}
void PLigacao::write(ostream &out) const {
	out << *ape;
}


ostream &operator <<(ostream &out, const PLigacao &e)
{
	e.write(out);
	return out;
}

#endif 
