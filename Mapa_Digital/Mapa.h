#ifndef _Mapa_
#define _Mapa_

#include <string>
#include "PLigacao.h"
#include "graphStlPath.h"
#include "Turistico.h"


class Mapa :  public graphStlPath <Turistico* , PLigacao>
{
private:
	void mostraCaminho(int ti, const vector <int> &path);
	void menorCaminho(Turistico* const &co,  Turistico* const &cf);
	list<Turistico*> Mapa::vertices(int vi, const vector <int> &path);
public:
	Mapa();
	Mapa(const Mapa &m);
	~Mapa();
	void inserirGrafo(Turistico* ori, Turistico* des, Ligacao* caminho);
	void caminhospossiveis(Turistico*const &li, Turistico* const &lf);
	void menorCaminhoDistancia(Turistico* const  &di, Turistico* const  &df);
	void menorCaminhoCusto(Turistico* const  &csi, Turistico* const  &csf);
	void calculartempoViagem(Turistico* const &tv, int tmpI, list<Turistico*> turt);
};

Mapa::Mapa() : graphStlPath <Turistico*, PLigacao>() {
}
Mapa::Mapa(const Mapa &m) : graphStlPath <Turistico*, PLigacao>(m) {
}

Mapa::~Mapa(){}

void Mapa::inserirGrafo(Turistico* ori , Turistico* des, Ligacao* caminho) {
	PLigacao ape(Autoestrada(caminho->getCodigo(),caminho->getTotalKm(),caminho->getTempoM(),caminho->getPrecoPortagem()));
	addGraphEdge(ape,ori,des);
	addGraphEdge(ape,des,ori);
}

void Mapa::caminhospossiveis(Turistico* const  &turi1, Turistico* const &turi2){

	queue <stack<Turistico*>> q=distinctPaths(turi1,turi2);
	cout << "Ligacao entre " <<turi1->getDescricao()<<" e "<<turi2->getDescricao()<<" :" <<endl;
	while(!q.empty())
	{
		stack<Turistico*> aux=q.front();
		list<Turistico*> laux;
		while(!aux.empty())
		{
			laux.push_front(aux.top());
			aux.pop();
		}
		PLigacao temp("",0,0,0);
		cout<<"________________->TRAJECTO<-_____________"<<endl;
		for(list<Turistico*>::iterator it1=laux.begin();it1!=laux.end();it1++)
		{
			cout<<(*it1)->getDescricao()<<endl;
			if((*it1)!=turi2)
			{
				PLigacao pl;
				this->getEdgeByVertexContents(pl,(*it1),(*it1++));
				temp+=pl;
				it1--;

				cout<< " ->"<<pl.getDesc() <<endl;
			}
		}
		q.pop();
		cout << endl;
		cout<<"_______________"<<endl;
		cout<<"Total de caminhos: "<<endl;
		cout<<temp<<endl;
		cout<<"_______________"<<endl;

	}
}



void Mapa::calculartempoViagem(Turistico* const &tv, int tempoI, list<Turistico*> turt) {

	PLigacao::setComparacaoTEMPO();
	PLigacao m("",0,0,0);
	list<Turistico*> tmp;
	vector<int> pathm;
	int Codmin;

	for (list<Turistico*>::iterator it1=turt.begin();it1!=turt.end();it1++)
	{
		vector <int> path; vector <PLigacao> dist; int cod;
		this->getVertexKeyByContent(cod, *it1);
		this->dijkstrasAlgorithm(tv, path, dist);
		if(m.getTempo()==0 || m>dist[cod])
		{
			m=dist[cod];
			pathm=path;
			Codmin=cod;
		}
	}
	tmp=vertices(Codmin,pathm);
}

void Mapa::mostraCaminho(int ti, const vector <int> &path) {
	if (path[ti]==-1) return;
	mostraCaminho(path[ti], path);
	PLigacao l;	this->getEdgeByVertexKeys(l, path[ti], ti);
	Turistico* tu; this->getVertexContentByKey(tu, ti);
	cout << " ---> " << l  << " ---> " << (tu)->getDescricao();
}

void Mapa::menorCaminho( Turistico* const &co , Turistico* const  &cf) {
	vector <int> path; vector <PLigacao> dst; int cod;
	this->getVertexKeyByContent(cod, cf);
	this->dijkstrasAlgorithm(co, path, dst);

	cout << dst[cod].getKM() << "km ; " << dst[cod].getPreco() << "EUR" << endl;
	cout << (co)->getDescricao();
	mostraCaminho(cod, path);
	cout << " "<< endl ;
	cout << " "<< endl ;
}

void Mapa::menorCaminhoDistancia( Turistico*  const &co, Turistico* const  &cf) {
	PLigacao::setComparacaoKMS();
	cout << "Caminho com Menor distancia entre " << (co)->getDescricao() << " e " << (cf)->getDescricao() << " : " ;
	menorCaminho(co, cf);
}


void Mapa::menorCaminhoCusto(Turistico* const  &csi, Turistico* const  &csf) {

	PLigacao::setComparacaoCUSTO();
	cout << "Caminho com Menor custo entre " << csi << " e " << csf << " : ";
	menorCaminho(csi, csf);
}


list<Turistico*> Mapa::vertices(int vi, const vector <int> &path)
{
	list<Turistico*> tu;
	if (path[vi]!=-1)
	{
		vertices(path[vi],  path);
		Turistico* t; this->getVertexContentByKey(t, vi);
		tu.push_back(t);
	}
	return tu;
}


#endif