#include "Ficheiro.h"

void menuInicio()
{
	cout << "\n";
	cout << "## MENU PRINCIPAL ##\n";
	cout << "## 1 - Carregar ficheiros\n";
	cout << "## 2 - Listar\n";
	cout << "## 3 - Sair\n";
}

void menuCarregar() {
	cout << "\n";
	cout << "## CARREGAR FICHEIROS ##\n";
	cout << "## 0- Carregar todos os ficheiros\n";
	cout << "## 1- Carregar Ficheiro de Interesse Turistico\n";
	cout << "## 2- Carregar Ficheiro de Vias de Ligacao\n";
	cout << "## 3- Retroceder\n";
}


void menuListar(){
	cout << "\n";
	cout << "## LISTAR ##\n";
	cout << "## 0 - Listar Interesse Turistico\n";
	cout << "## 1 - Listar Vias de Ligacao\n";
	cout << "## 2 - Contabilizar Locais de interesse Turistico\n";
	cout << "## 3 - Ordenacao por Ordem Alfabetica\n";
	cout << "## 4 - Criar Grafo\n";
	cout << "## 5 - Complexidade Temporal\n";
	cout << "## X - Retroceder\n";
}


void menucalcular(){
	cout << "\n";
	cout << "## CALCULAR PERCURSOS ENTRE DOIS LOCAIS TURISTICOS ##\n";
	cout << "\n";	
	cout << ">> 0- Mais curto em KMS \n";
	cout << ">> 1- Mais economico em EUROS \n";
	cout << ">> 2- Maior interesse Turistico\n";
	cout << ">> 3- Retroceder\n";
}



int main(int argc, const char * argv[])
{

	cout << "#########################################\n"; 
	cout << "          ### Mapa Digital ###\n"; 
	cout << "#########################################\n"; 
	cout << "## TRABALHO PRATICO ESTRUTURAS DE INFORMACAO ##\n"; 
	cout << "#########################################\n"; 
	cout << "\n";
	cout << "            REALIZADO POR:\n";
	cout << "\n";
	cout << "       V�tor Rodrigues - 1111230         \n"; 
	cout << "          Jos� Tavares - 1111228         \n"; 
	cout << "\n";
	cout << "           8 DEZEMBRO DE 2013            \n";
	cout << "\n";
	cout << "---------------> MENUS <---------------\n";

	bool exit=false, carregado=false;
	bool turistico=false, local=false, ficheiro=false;
	char *opcao = new char;
	int *menu = new int;
	*menu=1;
	Ficheiro f;
	string nomeT, nomeL;
	string nomeFich1,nomeFich2,nomeF1 = "fLocais", nomeF2 = "fVias";

	while(!exit)
		switch(*menu) {
		case 1:
			menuInicio();
			cin >> *opcao;
			switch(*opcao){
			case '1': *menu=2;
				break;
			case '2': *menu=3;
				break;
			case '3': *menu=4;
				exit=true;
				break;
			default:
				cout << "\n\n";
				cout << "Opcao invalida\n";
				cout << "\n\n";
				break;
			}
			break;
		case 2:
			menuCarregar();
			cin >> *opcao;
			switch(*opcao) {
			case '0':				
				cout <<""<< endl;				
				if(ficheiro || turistico && local == true){
					cout << "Ficheiros carregados com sucesso!\n";
					cout << "\n";
					*menu=1;
					break;					
				}
				else {
					cout << "Ficheiros nao carregados!\n ";
					cout << "\n";
				}

				//Turistico
				cout << "Nome do Ficheiro de Interesses Turistico? ";
				cin >> nomeFich1;
				while (nomeFich1 != nomeF1)
				{
					cout << "Por favor insira o nome correto do Ficheiro: ";					
					cin >> nomeFich1;
				}
				//Liga��o
				cout << "Nome do Ficheiro de Vias de Ligacao? ";
				cin >> nomeFich2;
				while (nomeFich2 != nomeF2)
				{
					cout << "Por favor insira o nome correto do Ficheiro: ";					
					cin >> nomeFich2;
				}
				f.lerTuristico(nomeFich1);

				cout << "Ficheiro de Interesses Turisticos Carregado com Sucesso!\n ";
				f.lerLigacao(nomeFich2);

				cout << "Ficheiro de Vias de Ligacao Carregado com Sucesso!\n ";
				ficheiro=true;
				*menu=1;
				break; 

			case '1':
				cout <<""<< endl;
				//Turistico				
				if(turistico && ficheiro == false)
				{			
					//InteresseTuristico
					cout << "Nome do Ficheiro de Vias de Ligacao? ";
					cin >> nomeFich1;
					while (nomeFich1 != nomeF1)
					{
						cout << "Por favor insira o nome correto do Ficheiro: ";					
						cin >> nomeFich1;
					}
					f.lerTuristico(nomeFich1);
					turistico=true;
					local=false;
					cout << "Ficheiro de Interesses Turisticos Carregado com Sucesso!\n ";
					*menu=1;
					break; 
				}
				else if(turistico || ficheiro == true){
					cout << "Ficheiros de Interesse Turistico ja carregados com Sucesso! ";
					cout << "\n";
					*menu=1;
					break;
				}			

			case '2': 
				cout <<""<< endl;
				//Liga��o				
				if(local && ficheiro == false)
				{						
					cout << "Nome do Ficheiro de Vias de Ligacao? ";
					cin >> nomeFich2;
					while (nomeFich2 != nomeF2)
					{
						cout << "Por favor insira o nome correto do Ficheiro: ";					
						cin >> nomeFich2;
					}
					f.lerLigacao(nomeFich2);
					local = true;
					turistico=false;					
					cout << "Ficheiro de Vias de Ligacao Carregado com Sucesso!\n ";
					*menu=1;
					break;
				}
				else if(local || ficheiro == true){
					cout << "Ficheiros de Vias de Ligacao ja carregados com Sucesso! ";
					cout << "\n";
					*menu=1;
					break;
				}

			case '3':
				*menu=1;
				break;

			case '4':
				*menu=1;
				break;
			default:
				cout << "\n\n";
				cout << "opcao invalida\n ";
				cout << "\n\n";
				break;
			}
			break;

		case 3:		
			menuListar();			
			cin >> *opcao;
			switch(*opcao) {
			case '0':
				//Listar Turistico
				if(ficheiro || turistico && local == true) {
					cout << "Ficheiros ja carregados!\n ";
					cout << "\n";
					f.listarTuristico();
					break;
				} 
				cout <<"\n";
				cout << "Imposs�vel listar Interesse Turistico!\n ";
				cout << "Os Ficheiros nao foram todos carregados!\n ";
				cout << "Por favor carregue os Ficheiros Primeiro!\n ";
				break;

			case '1':
				//Listar ligacao
				if(ficheiro || turistico && local == true) {
					cout << "Ficheiros ja carregados!\n ";
					cout << "\n";
					f.listarLigacao();
					break;
				} 
				cout <<"\n";
				cout << "Imposs�vel listar Interesse Turistico!\n ";
				cout << "Os Ficheiros nao foram todos carregados!\n ";
				cout << "Por favor carregue os Ficheiros Primeiro!\n ";
				break;


			case '2':
				//Contar
				if(ficheiro || turistico && local == true) {
					cout << "Ficheiros ja carregados!\n ";
					cout << "\n";
					f.contar(); 
					break;
				}
				cout <<"\n";
				cout << "Imposs�vel listar Interesse Turistico!\n ";
				cout << "Os Ficheiros nao foram todos carregados!\n ";
				cout << "Por favor carregue os Ficheiros Primeiro!\n ";
				break;

			case '3':
				//Ordenar
				if(ficheiro || turistico && local == true) {
					cout << "Ficheiros ja carregados! \n";
					cout << "\n";
					f.ordenar(); f.listarTuristico();
					break;
				}
				cout <<"\n";
				cout << "Imposs�vel listar Interesse Turistico!\n ";
				cout << "Os Ficheiros nao foram todos carregados!\n";
				cout << "Por favor carregue os Ficheiros Primeiro!\n";
				break;

			case '4':
				//Ordenar com o sort list
				if(ficheiro || turistico && local == true) {
					cout << "Ficheiros ja carregados!\n ";
					cout << "\n";
					f.criarGrafo(); cout << f;
					break;
				}
				cout <<"\n";
				cout << "Imposs�vel listar Interesse Turistico!\n ";
				cout << "Os Ficheiros nao foram todos carregados!\n ";
				cout << "Por favor carregue os Ficheiros Primeiro!\n ";
				break;

			case '5':
				//Complexidade temporal
				if(ficheiro || turistico && local == true) {
					cout << "Ficheiros ja carregados!\n ";
					cout << "\n";
					cout << "Complexidade temporal: N^2" << endl;
					break;
				}
				cout <<"\n";
				cout << "Imposs�vel listar Interesse Turistico!\n ";
				cout << "Os Ficheiros nao foram todos carregados!\n ";
				cout << "Por favor carregue os Ficheiros Primeiro!\n ";
				break;

			case '6':
				//Complexidade temporal
				if(ficheiro || turistico && local == true) {
					cout << "Ficheiros ja carregados!\n ";
					cout << "\n";
					cout << "Complexidade temporal: N^2" << endl;
					break;
				}
				cout <<"\n";
				cout << "Imposs�vel listar Interesse Turistico!\n ";
				cout << "Os Ficheiros nao foram todos carregados!\n ";
				cout << "Por favor carregue os Ficheiros Primeiro!\n ";
				break;

			case '7':
				//Percursos possiveis entre 2 locais
				if(ficheiro || turistico && local == true) {
					cout << "Ficheiros ja carregados!\n ";
					cout << "\n";
					//f.caminhospossiveis();
					break;
				}
				cout <<"\n";
				cout << "Imposs�vel listar Interesse Turistico!\n ";
				cout << "Os Ficheiros nao foram todos carregados!\n ";
				cout << "Por favor carregue os Ficheiros Primeiro!\n ";
				break;

			case '8':
				//Calcular
				menucalcular();
				cin >> *opcao;
				switch(*opcao) {
				case '0':
					//Mais curto em Kms
					if(ficheiro || turistico && local == true) {
						cout << "Ficheiros ja carregados!\n ";
						cout << "\n";
						//f.menorCaminhoCusto();
						break;
					}
					cout <<"\n";
					cout << "Imposs�vel listar Interesse Turistico!\n ";
					cout << "Os Ficheiros nao foram todos carregados!\n ";
					cout << "Por favor carregue os Ficheiros Primeiro!\n ";
					break;

				case '1':
					// Mais economico em euros
					if(ficheiro || turistico && local == true) {
						cout << "Ficheiros ja carregados!\n ";
						cout << "\n";
						//f.menorCaminhoCusto();
						break;
					}
					cout <<"\n";
					cout << "Imposs�vel listar Interesse Turistico!\n ";
					cout << "Os Ficheiros nao foram todos carregados!\n ";
					cout << "Por favor carregue os Ficheiros Primeiro!\n ";
					break;

				case '2':
					//Maior interesse turistico
					if(ficheiro || turistico && local == true) {
						cout << "Ficheiros ja carregados!\n ";
						cout << "\n";
						//f.ordenar(); f.listarTuristico();
						break;
					}
					cout <<"\n";
					cout << "Imposs�vel listar Interesse Turistico!\n ";
					cout << "Os Ficheiros nao foram todos carregados!\n ";
					cout << "Por favor carregue os Ficheiros Primeiro!\n ";
					break;

				case '3':
					*menu=3;
					break;
				default:
					cout << "\n\n";
					cout << "opcao invalida\n ";
					cout << "\n\n";
					break;
				}
				//}
				break;
				*menu=1;
			case '9':
				//Ultimo pedido do enunciado
				if(ficheiro || turistico && local == true) {
					cout << "Ficheiros ja carregados!\n ";
					cout << "\n";
					//f.ordenar(); f.listarTuristico();
					break;
				}
				cout <<"\n";
				cout << "Imposs�vel listar Interesse Turistico!\n ";
				cout << "Os Ficheiros nao foram todos carregados!\n ";
				cout << "Por favor carregue os Ficheiros Primeiro!\n ";
				break;		
			case 'X':
				*menu=1;
				break;
			default:
				cout << "\n\n";
				cout << "opcao invalida\n ";
				cout << "\n\n";
				break;
			}			
			break;
			*menu=1;
	}
	return 0;
}
