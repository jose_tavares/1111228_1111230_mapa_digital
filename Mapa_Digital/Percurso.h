#ifndef Percurso_
#define Percurso_

#include "Turistico.h"
#include "Ligacao.h"

class Percurso 
{
private:
	Turistico* origem;
	Turistico* destino;
	Ligacao* percurso;

public:
	Percurso();
	Percurso(Turistico* o, Turistico* d, Ligacao* p);
	Percurso(const Percurso& p);
	virtual ~Percurso();

	void setOrigem(Turistico* o);
	void setDestino(Turistico* d);
	void setPercurso(Ligacao* p);

	Turistico* getOrigem() const;
	Turistico* getDestino() const;
	Ligacao* getPercurso() const;

	Percurso *clone() const ;
	bool Percurso :: operator==(const Percurso& cp) const;
	Percurso& operator=(const Percurso& cp);

	void escreve(ostream &out) const;

};

Percurso::Percurso()
{
	origem = 0;
	destino = 0;
	percurso = 0;
}

Percurso::Percurso(Turistico* o, Turistico* d, Ligacao* p)
{
	origem = o;
	setDestino(d);
	setPercurso(p);
}

Percurso::Percurso(const Percurso &p)
{
	setOrigem(p.origem);
	setDestino(p.destino);
	setPercurso(p.percurso);
}

Percurso::~Percurso(){};

void Percurso::setOrigem(Turistico* o)
{
	origem = o;
}

void Percurso::setDestino(Turistico* d)
{
	destino = d;
}

void Percurso::setPercurso(Ligacao* p)
{
	percurso = p;
}

Turistico* Percurso::getOrigem() const
{
	return origem;
}

Turistico* Percurso::getDestino() const
{
	return destino;
}

Ligacao* Percurso::getPercurso() const
{
	return percurso;
}

Percurso& Percurso::operator = (const Percurso& cp) 
{ 
	if(this != &cp)
	{
		setOrigem(cp.origem);
		setDestino(cp.destino);
		setPercurso(cp.percurso);
	}
	return *this; 
}

bool Percurso::operator==(const Percurso& cp) const
{ 
	return(percurso == cp.percurso && *origem==*cp.origem && *destino==*cp.destino);
}

Percurso * Percurso:: clone() const{
	return new Percurso(*this);
}

void Percurso::escreve(ostream &out) const{

	out << "Ligacao entre " << origem -> getDescricao() << " a " << destino -> getDescricao() << endl;
	percurso -> escreve(out);
}

ostream & operator << (ostream &out, const Percurso &p){
    p.escreve(out);
    return out;
}

#endif // !Percurso_