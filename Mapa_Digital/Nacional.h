#ifndef NACIONAL_
#define NACIONAL_

#include "Ligacao.h"

#include <string>
#include <iostream>

using namespace std;

class Nacional : public Ligacao
{
private:
	//variavel
	string pavimento;


public:
	//Construtor e Distrutor
	Nacional();
	Nacional(string cd, int tk, int tm, string pv);
	Nacional(const Nacional &n);

	//Metodo clone
	virtual Ligacao * clone() const;

	//Metodo acesso set
	void setPavimento(string pv);

	//Metodo acesso get
	string getPavimento();

	//Metodo escreve
	virtual void escreve(ostream &out) const;
};

//Construtores
Nacional::Nacional()
{
	pavimento = "";
}

Nacional::Nacional(string cd, int tk, int tm, string pv) : Ligacao(cd,tk,tm)
{
	pavimento = pv;
}

Nacional::Nacional(const Nacional &n) :  Ligacao(n)
{
	pavimento = n.pavimento;
}

//Metodo clone
Ligacao* Nacional::clone() const
{
	return new Nacional(*this);
}

//Metodos de acesso set
void Nacional::setPavimento(string pv)
{
	pavimento = pv;
}

//Metodos de acesso get
string Nacional::getPavimento()
{
	return pavimento;
}

//Metodo escreve
void Nacional::escreve(ostream &out) const
{
	Ligacao::escreve(out);
	out << "Pavimento: "<< pavimento << endl;
}

ostream & operator << (ostream &out, const Nacional &n){
	n.escreve(out);
	return out;
}

#endif
