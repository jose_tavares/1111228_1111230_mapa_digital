#ifndef Autoestrada_
#define Autoestrada_

#include "Ligacao.h"

#include <string>
#include <iostream>

using namespace std;


class Autoestrada : public Ligacao
{
private:
	//variavel
	double precoPortagem;


public:
	//Construtor e Distrutor
	Autoestrada();
	Autoestrada(string cd, int tk, int tm, double prp);
	Autoestrada(const Autoestrada &a);

	//Metodo clone
	virtual Ligacao * clone() const;

	//Metodo acesso set
	virtual void setPrecoPortagem(double prp);

	//Metodo acesso get
	virtual double getPrecoPortagem() const;

	//Metodo escreve
	virtual void escreve(ostream &out) const;
};

//construtores
Autoestrada::Autoestrada()
{
	precoPortagem = 0;
}

Autoestrada::Autoestrada(string cd, int tk, int tm, double prp):Ligacao(cd, tk, tm)
{
	precoPortagem = prp;
}

Autoestrada:: Autoestrada (const Autoestrada &a) : Ligacao(a)
{
	precoPortagem = a.precoPortagem;
}

//Metodo clone
Ligacao* Autoestrada::clone() const
{
	return new Autoestrada(*this);
}

//Metodos de acesso set
void Autoestrada::setPrecoPortagem(double prp)
{
	precoPortagem = prp;
}


//Metodos de acesso get
double Autoestrada::getPrecoPortagem() const
{
	return precoPortagem;
}


//Metodo escreve
void Autoestrada::escreve(ostream &out) const
{
  Ligacao :: escreve(out);
  out << "Preco de Portagem: "<< precoPortagem << " euros " << endl;
}

ostream & operator << (ostream &out, const Autoestrada &a){
    a.escreve(out);
    return out;
}


#endif