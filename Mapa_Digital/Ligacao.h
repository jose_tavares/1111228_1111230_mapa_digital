#ifndef Ligacao_
#define Ligacao_

#include <iostream>
#include <string>

#include "Turistico.h"

using namespace std;

class Ligacao
{
private:
	//variaveis
	string codigo;
	int totalKm;
	int tempoM;

public:
	//Construtor e Distrutor
	Ligacao();
	Ligacao(string cd, int tk, int tm);
	Ligacao(const Ligacao &l);

	//Metodo clone
	virtual Ligacao * clone() const = 0;

	//Metodo acesso set
	void setCodigo(string cd);
	void setTotalKm(int tk);
	void setTempoM(int tm);

	//Metodo escreve
	virtual void escreve(ostream &out) const;

	//Metodo acesso get
	string getCodigo() const;
	int getTotalKm() const;
	int getTempoM() const;
	virtual double getPrecoPortagem() const;
};


//Construtores
Ligacao::Ligacao()
{
	codigo = "";
	totalKm = 0;
	tempoM = 0;
}


Ligacao::Ligacao (string cd, int tk, int tm)
{
	codigo = cd;
	totalKm = tk;
	tempoM = tm;
}

Ligacao::Ligacao(const Ligacao &l)
{
	codigo = l.codigo;
	totalKm = l.totalKm;
	tempoM = l.tempoM;
}

//Metodos de acesso set
void Ligacao::setCodigo(string cd)
{
	codigo = cd;
}


void Ligacao::setTotalKm(int tk)
{
	totalKm = tk;
}


void Ligacao::setTempoM(int tm)
{
	tempoM = tm;
}

double Ligacao::getPrecoPortagem() const {
	return 0;
}

//Metodo escreve
void Ligacao::escreve(ostream &out) const
{
	out << "Codigo: " << codigo << endl;
	out << "Distancia: " << totalKm << " km" << endl;
	out << "Tempo Medio: " << tempoM << " minutos" << endl;
}


//Metodos de acesso get
string Ligacao::getCodigo() const
{
	return codigo;
}


int Ligacao:: getTotalKm() const
{
	return totalKm;
}


int Ligacao:: getTempoM() const
{
	return tempoM;
}

ostream& operator << (ostream &out, const Ligacao &l)
{
	l.escreve(out);
	return out;
}


#endif