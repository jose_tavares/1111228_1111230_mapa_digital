#ifndef Turistico_
#define Turistico_


#include <iostream>
#include <string>

using namespace std;


class Turistico
{
private:
	//variavel
	string descricao;

public:
	//Construtor e Distrutor
	Turistico();
	Turistico(string dc);
	Turistico(const Turistico &t);
	virtual ~Turistico();

	//Metodo clone
	virtual Turistico * clone() const;

	//Metodo acesso set
	void setDescricao(string dc);

	//Metodo acesso get
	string getDescricao() const;

	//Metodo escreve
	virtual void escreve(ostream &out) const;

	//Sobrecarga operadores
	virtual Turistico &operator = (const Turistico &l);
	bool operator == (const Turistico &l) const;
	bool operator > (const Turistico &l) const;

};


//Construtores
Turistico::Turistico()
{
	descricao = "";
	
}

Turistico::Turistico(string dc) : descricao(dc)
{
	descricao = dc;
}


Turistico::Turistico(const Turistico &t) 
{
	descricao = t.descricao;
}


//Distrutor
Turistico::~Turistico()
{

}


//Metodo clone
Turistico* Turistico::clone() const
{
	return new Turistico(*this);
}



//Metodos de acesso set
void Turistico::setDescricao(const string dc)
{
	descricao = dc;
}


//Metodos de acesso get
string Turistico::getDescricao() const
{
	return descricao;
}


//Metodo escreve
void Turistico::escreve(ostream &out) const
{
	out << "Descricao: " << descricao << endl;
}


//Sobrecarga operadores
bool Turistico::operator == (const Turistico &t) const
{
	if(descricao == t.descricao) {
			return true;
	}else {
		return false;
	}
}



bool Turistico::operator > (const Turistico &t) const
{
	return (descricao > t.descricao);
}


Turistico& Turistico:: operator = (const Turistico &t)
{
	setDescricao(t.descricao);
	

	return *this;
}


ostream& operator << (ostream &out, const Turistico &t)
{
  t.escreve(out);
  return out;
}


//int Turistico::getAbertura() const {return 0};
//int Turistico::getFecho() const {return 0};
//int Turistico::tempo_med() const {return 0};

#endif