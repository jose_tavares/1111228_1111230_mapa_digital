#ifndef Historico_Culturais_
#define Historico_Culturais_

#include "Turistico.h"

#include <string>
#include <iostream>

using namespace std;

class Historico_Culturais : public Turistico
{

private:
	//variavel
	int tempoMvisita;
	int horarioAbertura;
	int horarioEncerramento;

public:
	//Construtor e Distrutor
	Historico_Culturais();
	Historico_Culturais(string dc, int tv, int ha, int he);
	Historico_Culturais( const Historico_Culturais &hc);
	virtual ~Historico_Culturais();

	//Metodo clone
	Historico_Culturais * clone() const;

	//Metodo acesso set
	void setTempoMVisita(int tv);

	//Metodo acesso set
	void setHorarioAbertura(int ha);

	//Metodo acesso set
	void setHorarioEncerramento(int he);

	//Metodo acesso get
	int getTempoMVisita();

	//Metodo acesso get
	int getHorarioAbertura();

	//Metodo acesso get
	int getHorarioEncerramento();

	//Metodo escreve
	void escreve(ostream &out) const;

	//Sobrecarga operadores
	bool operator == (const Historico_Culturais &hc) const;
	bool operator > (const Historico_Culturais &hc) const;
	
	const Historico_Culturais& operator = (const Historico_Culturais &hc);

};

Historico_Culturais::Historico_Culturais()
{
	tempoMvisita = 0;
	horarioAbertura = 0;
	horarioEncerramento = 0;
}


Historico_Culturais::Historico_Culturais(string dc, int tv, int ha, int he) : Turistico(dc)
{
	tempoMvisita = tv;
	horarioAbertura = ha;
	horarioEncerramento = he;
}


Historico_Culturais::Historico_Culturais(const Historico_Culturais &hc) :  Turistico(hc)
{
	tempoMvisita = hc.tempoMvisita;
	horarioAbertura = hc.horarioAbertura;
	horarioEncerramento = hc.horarioEncerramento;
}


//Distrutor
Historico_Culturais::~Historico_Culturais()
{

}


//Metodo clone
Historico_Culturais* Historico_Culturais::clone() const
{
	return new Historico_Culturais(*this);
}


//Metodos de acesso set
void Historico_Culturais::setTempoMVisita(int tv)
{
	tempoMvisita = tv;
}


//Metodos de acesso set
void Historico_Culturais::setHorarioAbertura(int ha)
{
	horarioAbertura = ha;
}


//Metodos de acesso set
void Historico_Culturais::setHorarioEncerramento(int he)
{
	horarioEncerramento = he;
}


//Metodos de acesso get
int Historico_Culturais::getTempoMVisita()
{
	return tempoMvisita;
}


//Metodos de acesso get
int Historico_Culturais::getHorarioAbertura()
{
	return horarioAbertura;
}


//Metodos de acesso get
int Historico_Culturais::getHorarioEncerramento()
{
	return horarioEncerramento;
}


//Metodo escreve
void Historico_Culturais::escreve(ostream &out) const
{
  out << "HISTORICO E CULTURAL: " << endl;
 Turistico::escreve(out);
  out << "Tempo de Visita: "<< tempoMvisita << " minutos" << endl;
  out << "Horario de Abertura: "<< horarioAbertura << " minutos" << endl;
  out << "Horario de Encerramento: "<< horarioEncerramento << " minutos" << endl;
}


//Sobrecarga operadores
bool Historico_Culturais::operator == (const Historico_Culturais &hc) const
{
	return (Turistico::operator == (hc) && tempoMvisita == hc.tempoMvisita && horarioAbertura == hc.horarioAbertura && horarioEncerramento == hc.horarioEncerramento);
}


bool Historico_Culturais::operator > (const Historico_Culturais &hc) const
{
	return (Turistico::operator > (hc) && tempoMvisita > hc.tempoMvisita && horarioAbertura > hc.horarioAbertura && horarioEncerramento > hc.horarioEncerramento);
}


const Historico_Culturais& Historico_Culturais::operator = (const Historico_Culturais &hc)
{
	Turistico::operator=(hc);
  tempoMvisita = hc.tempoMvisita;
  horarioAbertura = hc.horarioAbertura;
  horarioEncerramento = hc.horarioEncerramento;
  
  return *this;
}


ostream & operator << (ostream &out, const Historico_Culturais &hc){
    hc.escreve(out);
    return out;
}


#endif