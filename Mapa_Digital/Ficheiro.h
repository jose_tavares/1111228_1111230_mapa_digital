#ifndef FICHEIRO_
#define FICHEIRO_

#include <iostream> 
#include <fstream> 
#include <string> 
#include <vector> 
#include <list>
#include <stdio.h>

using namespace std;

#include "Naturais.h"
#include "Historico_Culturais.h"
#include "Autoestrada.h"
#include "Nacional.h"
#include "Ligacao.h"
#include "Turistico.h"
#include "Percurso.h"
#include "Mapa.h"


class Ficheiro : public Mapa
{
private:
	list <Turistico*> vecTuristico; 
	list <Percurso*> vecLigacao;

	static bool comparar(Turistico *t1, Turistico *t2);


public:
	int lerTuristico(string f);
	int lerLigacao(string f);

	list <Percurso *> getVecLigacao() const;
	list <Turistico *> getVecTuristico() const;

	Ficheiro();
	Ficheiro(const Ficheiro &f);
	~Ficheiro();
	void inserirTuristico(Turistico *t); 
	void inserirLigacao(Percurso *l);
	void listarLigacao(); 
	void listarTuristico();
	void output(ostream &out)const;
	Turistico *findTuristico(string);
	Percurso *findLigacao(string);
	void ordenar();
	void contar();

	void criarGrafo();

};

Ficheiro::Ficheiro() 
{ 
}

Ficheiro::Ficheiro(const Ficheiro &f) 
{ 
	(*this).vecTuristico.assign(f.vecTuristico.begin(), f.vecTuristico.end()); 
	(*this).vecLigacao.assign(f.vecLigacao.begin(), f.vecLigacao.end()); 
}


Ficheiro::~Ficheiro() 
{ 
	vecTuristico.clear(); 
	vecLigacao.clear(); 
}

list <Percurso *> Ficheiro:: getVecLigacao() const
{
	return vecLigacao;
}

list <Turistico*> Ficheiro :: getVecTuristico() const
{
	return vecTuristico;
}

bool Ficheiro::comparar(Turistico *t1, Turistico *t2)
{
	return t1 -> getDescricao() < t2 -> getDescricao();
}

void Ficheiro::ordenar()
{
	vecTuristico.sort(comparar);
}

void Ficheiro::inserirTuristico(Turistico *t) 
{ 
	bool flag = false;

	for(list<Turistico*>::iterator it=vecTuristico.begin(); 
		it != vecTuristico.end(); it++)
	{
		if(*(*it) == *t)
			flag = true;
	}

	if(flag == false) vecTuristico.push_back(t->clone());
	else cout << "Local existente!!!" << endl;
}


void Ficheiro::inserirLigacao(Percurso *l) 
{ 
	bool flag = false;

	for(list<Percurso*>::iterator it=vecLigacao.begin(); 
		it != vecLigacao.end(); it++)
	{
		if(*(*it) == *l)
			flag = true;
	}

	if(flag == false) vecLigacao.push_back(l->clone());
	else cout << "Ligacao existente!!!" << endl;
};


void Ficheiro::listarTuristico() 
{ 
	for(list<Turistico*>::iterator it=vecTuristico.begin(); 
		it != vecTuristico.end(); it++)
	{ 
		cout << *(*it) <<endl; 
	} 
}


void Ficheiro::listarLigacao() 
{ 
	for (list<Percurso*>::iterator it=vecLigacao.begin(); 
		it != vecLigacao.end(); it++) 

		cout << (*(*it)) << endl ; 
}

Turistico* Ficheiro::findTuristico(string s)
{ 
	for(list<Turistico*>::iterator it=vecTuristico.begin(); 
		it != vecTuristico.end(); it++)

		if ((*(*it)).getDescricao().compare(s) == 0) return *it;
	return NULL;
} 

Percurso* Ficheiro::findLigacao(string s)
{ 
	for(list<Percurso*>::iterator it=vecLigacao.begin(); 
		it != vecLigacao.end(); it++)

		if ((*(*it)).getPercurso() -> getCodigo().compare(s) == 0) return *it;
	return NULL;
} 

void Ficheiro::contar()
{
	cout << "Total Locais: " << vecTuristico.size() << endl;

	int numNaturais = 0;
	int numHistoricoCult = 0;

	for(list<Turistico*>::iterator it = vecTuristico.begin(); it != vecTuristico.end(); it++)
	{
		if(typeid(*(*it)) == typeid(Historico_Culturais))
			numHistoricoCult++;
		if(typeid(*(*it)) == typeid(Naturais))
			numNaturais++;
	}

	cout << "Total interesses historico e cultural: " << numHistoricoCult << endl;
	cout << "Total interesses naturais: " << numNaturais << endl;
}

int Ficheiro::lerTuristico(string f)
{
	ifstream file;
	file.open(f); //define vari�vel e abre ficheiro para leitura (fLocais)

	string linha;
	int inic = 0;

	while (!file.eof())
	{
		getline(file, linha, '\n');
		if(linha.size() > 0)
		{
			int pos = linha.find(',', inic);
			string localT = (linha.substr(inic, pos-inic));
			pos++;

			inic = pos;
			pos = linha.find(',', inic);

			if(pos < 3)
			{
				string areaT = (linha.substr(inic, pos-inic));
				pos++;
				char* aux = &areaT[0];
				int area = atoi(aux);

				Naturais* n = new Naturais(localT, area);
				(*this).inserirTuristico(n);
			}
			else
			{
				string abert = (linha.substr(inic, pos-inic));
				pos++;
				char* aux2 = &abert[0];
				int abertura = atoi(aux2);

				inic = pos;
				pos = linha.find(',', inic);

				string inicio = (linha.substr(inic, pos-inic));
				pos++;
				char* aux3 = &inicio[0];
				int inicio1 = atoi(aux3);

				inic = pos;
				pos = linha.find(',', inic);

				string fim = (linha.substr(inic, pos-inic));
				pos++;
				char* aux4 = &fim[0];
				int fim1 = atoi(aux4);

				inic = pos;
				pos = linha.find(',', inic);

				Historico_Culturais* hc = new Historico_Culturais(localT, abertura, inicio1, fim1);
				inserirTuristico(hc);

			}

			inic = 0;
		}
	}

	file.close();
	return 0;
}

int Ficheiro::lerLigacao(string f)
{
	Percurso* p;
	ifstream file;
	bool flag = false;

	file.open(f); //define vari�vel e abre ficheiro para leitura (fVias)

	string linha;
	int inic = 0;

	while (!file.eof())
	{
		getline(file, linha, '\n');
		if(linha.size() > 0)
		{
			int pos = linha.find(',', inic);
			string localT1 = linha.substr(inic, pos-inic);
			pos++;

			inic = pos;
			pos = linha.find(',', inic);
			string localT2 = (linha.substr(inic, pos-inic));
			pos++;

			inic = pos;
			pos = linha.find(',', inic);
			string cod = (linha.substr(inic, pos-inic));
			pos++;

			inic = pos;
			pos = linha.find(',', inic);
			string txtKm = (linha.substr(inic, pos-inic));
			char* auxKm = &txtKm[0];
			int km = atoi(auxKm);
			pos++;

			inic = pos;
			pos = linha.find(',', inic);
			string txtTempo = (linha.substr(inic, pos-inic));
			char* auxTempo = &txtTempo[0];
			int tempo = atoi(auxTempo);
			pos++;

			inic = pos;
			pos = linha.find(',', inic);
			string txtAux = (linha.substr(inic, pos-inic));

			for(int i = 0; i < txtAux.size(); i++)
			{
				if(txtAux[i] >= '0' && txtAux[i] <= '9')
				{
					flag = true;
				}

			}

			if(flag)
			{
				char* auxAux = &txtAux[0];
				pos++;
				double preco = atof(auxAux);

				Autoestrada* a = new Autoestrada(cod, km, tempo, preco);
				p = new Percurso(new Turistico(localT1), new Turistico(localT2), a);
				(*this).inserirLigacao(p);
			}
			else
			{
				string pav = txtAux;
				pos++;

				Nacional* nac = new Nacional(cod, km, tempo, pav);
				p = new Percurso(new Turistico(localT1), new Turistico(localT2), nac);
				(*this).inserirLigacao(p);
			}

			inic = 0;
		}
	}

	file.close();
	return 0;
}

void Ficheiro :: criarGrafo()
{

	if(!vecTuristico.empty() && !vecLigacao.empty())
	{
		for (list<Percurso*>::iterator it=vecLigacao.begin(); it != vecLigacao.end(); it++) 
		{
			inserirGrafo((*it) -> getOrigem(), (*it) -> getDestino(), (*it) -> getPercurso());
		}

		for(list<Turistico*>::iterator it=vecTuristico.begin(); it != vecTuristico.end(); it++){
			addGraphVertex(*it);
		}
	}
}
#endif