#ifndef NATURAIS_
#define NATURAIS_

#include "Turistico.h"

#include <string>
#include <iostream>

using namespace std;


class Naturais : public Turistico
{

private:
	//variavel
	int area;

public:
	//Construtor e Distrutor
	Naturais();
	Naturais(string dc, int ar);
	Naturais(const Naturais &na);
	virtual ~Naturais();

	//Metodo clone
	Naturais * clone() const;

	//Metodo acesso set
	void setArea(int ar);

	//Metodo acesso get
	int getArea();

	//Metodo escreve
	void escreve(ostream &out) const;

	//Sobrecarga operadores
	bool operator == (const Naturais &na) const;
	bool operator > (const Naturais &na) const;

	const Naturais& operator = (const Naturais &na);

};

Naturais::Naturais()
{
	area = 0;
}


Naturais:: Naturais(string dc, int ar) : Turistico(dc)
{
	area = ar;
}


Naturais::Naturais(const Naturais &na) :  Turistico(na)
{
	area = na.area;
}


//Distrutor
Naturais::~Naturais()
{

}


//Metodo clone
Naturais* Naturais::clone() const
{
	return new Naturais(*this);
}


//Metodos de acesso set
void Naturais::setArea(int ar)
{
	area = ar;
}


//Metodos de acesso get
int Naturais::getArea()
{
	return area;
}


//Metodo escreve
void Naturais::escreve(ostream &out) const
{
	out << "NATURAL: " << endl;
	Turistico::escreve(out);
	out << "Area: "<< area << " km^2" << endl;
}


//Sobrecarga operadores
bool Naturais::operator == (const Naturais &na) const
{
	return (Turistico::operator == (na) && area == na.area);
}


bool Naturais::operator > (const Naturais &na) const
{
	return (Turistico::operator > (na) && area > na.area);
}


const Naturais& Naturais::operator = (const Naturais &na)
{
	Turistico::operator=(na);
	area = na.area;

	return *this;
}


ostream & operator << (ostream &out, const Naturais &na){
	na.escreve(out);
	return out;
}


#endif